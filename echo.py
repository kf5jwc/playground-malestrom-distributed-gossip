#!/usr/bin/env python3.9

import sys
from typing import Union, Optional, Literal
from functools import partial
from pydantic import BaseModel, Field, constr

NodeID = constr(to_lower=True, regex=r"[a-zA-Z]+\d+")

####


class Message(BaseModel):
    type: str
    msg_id: int


class MessageReply(Message):
    in_reply_to: int


####


class MessageInit(Message):
    type: Literal["init"]
    node_id: Optional[NodeID]
    node_ids: Optional[list[NodeID]]


class MessageInitReply(MessageReply):
    type: Literal["init_ok"] = "init_ok"


####


class MessageEcho(Message):
    type: Literal["echo"]
    echo: str


class MessageEchoReply(MessageReply):
    echo: str
    type: Literal["echo_ok"] = "echo_ok"


####


class Frame(BaseModel):
    src: NodeID
    dest: NodeID
    body: Union[MessageInit, MessageEcho, MessageInitReply, MessageEchoReply]


####


def main():
    print("Started", file=sys.stderr)

    NODE_ID: NodeID = NodeID("Z0")
    NODES: list[NodeID] = []
    msg_id = iter(range(1, 999999999999))

    for frame in map(Frame.parse_raw, sys.stdin):
        print("Received", frame.json(), file=sys.stderr)
        msg = frame.body

        if isinstance(msg, MessageInit):
            NODE_ID = frame.dest
            if msg.node_ids is not None:
                NODES = msg.node_ids
            reply = MessageInitReply(in_reply_to=msg.msg_id, msg_id=next(msg_id))

        if isinstance(msg, MessageEcho):
            reply = MessageEchoReply(in_reply_to=msg.msg_id, msg_id=next(msg_id), echo=msg.echo)

        try:
            frame_reply = Frame(src=NODE_ID, dest=frame.src, body=reply)
            print("Sending ", frame_reply.json(), file=sys.stderr)
            print(frame_reply.json(), flush=True)
        except NameError:
            print("Error, no reply was created", file=sys.stderr)
        else:
            del frame_reply

        try:
            del reply
        except NameError:
            pass

        del msg

    print("Ended", file=sys.stderr)


if __name__ == "__main__":
    main()
