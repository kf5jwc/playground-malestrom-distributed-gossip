#!/usr/bin/env python3.9

import sys
from typing import Union, Optional, Callable, Mapping, Literal
from loguru import logger
from pydantic import parse_raw_as
import echo
from echo import (
    NodeID,
    Message,
    MessageReply,
    MessageInit,
    MessageInitReply,
    MessageEcho,
    MessageEchoReply,
)


class MessageTopology(Message):
    type: Literal["topology"]
    topology: Mapping[NodeID, list[NodeID]]


class MessageTopologyReply(MessageReply):
    type: Literal["topology"] = "topology_ok"


class MessageBroadcast(Message):
    type: Literal["broadcast"]
    message: int


class MessageBroadcastSync(Message):
    type: Literal["broadcast_sync"] = "broadcast_sync"
    message: int
    informed: set[NodeID]


class MessageBroadcastReply(MessageReply):
    type: Literal["broadcast_ok"] = "broadcast_ok"


class MessageRead(Message):
    type: Literal["read"]


class MessageReadReply(MessageReply):
    type: Literal["read_ok"] = "read_ok"
    messages: list[int]

####


class Frame(echo.Frame):
    body: Union[
        MessageInit,
        MessageInitReply,
        MessageTopology,
        MessageTopologyReply,
        MessageEcho,
        MessageEchoReply,
        MessageBroadcast,
        MessageBroadcastSync,
        MessageBroadcastReply,
        MessageRead,
        MessageReadReply,
    ]


class FrameWithID(Frame):
    id: int


####


class UninitializedNodeError(Exception):
    pass


class NoReply(Exception):
    pass


####


class Operation:
    node_id: Optional[NodeID] = None
    nodes: list[NodeID] = []
    neighbors: set[NodeID] = set()
    reply_to: {type: Callable} = {}
    msg_log_sent: Mapping[int, Frame] = {}
    msg_log_broadcasts: set[int] = set()

    def __init__(self):
        self.reply_to = {
            MessageInit: self.initialize,
            MessageEcho: self.echo,
            MessageTopology: self.topology,
            MessageBroadcast: self.broadcast,
            MessageBroadcastSync: self.broadcast_sync,
            MessageRead: self.read,
        }

    def __call__(self, raw_msg: str) -> Frame:
        logger.trace(f"Message recv'd (raw): {raw_msg}")
        frame = parse_raw_as(Union[Frame, FrameWithID], raw_msg)

        if self.node_id is None and type(frame.body) is not MessageInit:
            logger.warning("Node is not initialized!")
            raise UninitializedNodeError(frame)

        logger.debug(f"Received {frame.body.__class__.__name__}")
        logger.trace(f"Received {frame}")
        reply = self.reply_to[type(frame.body)](frame)
        if reply is None:
            raise NoReply

        self.msg_log_sent[reply.msg_id] = Frame(
            src=self.node_id, dest=frame.src, body=reply
        )

        logger.debug(f"Sending {reply.__class__.__name__}")
        logger.trace(f"Sending {self.msg_log_sent[reply.msg_id]}")
        return self.msg_log_sent[reply.msg_id]

    @property
    def next_msg_id(self) -> int:
        try:
            return max(self.msg_log_sent.keys())
        except ValueError:
            return 1

    ####

    def initialize(self, frame) -> MessageInitReply:
        if self.node_id is not None:
            logger.info(f"Node renamed from {self.node_id} to {frame.dest}")
        else:
            logger.info(f"Node initialized as {frame.dest}")

        self.node_id = frame.dest

        if frame.body.node_ids is not None:
            self.nodes = frame.body.node_ids

        return MessageInitReply(msg_id=self.next_msg_id, in_reply_to=frame.body.msg_id)

    def echo(self, frame) -> MessageEchoReply:
        msg = frame.body
        return MessageEchoReply(
            msg_id=self.next_msg_id, in_reply_to=msg.msg_id, echo=msg.echo
        )

    def topology(self, frame) -> MessageTopologyReply:
        self.neighbors = set(frame.body.topology[self.node_id])
        logger.debug("Loaded neighbors")
        logger.trace(f"Neighbors: {self.neighbors}")

        if self.node_id in self.neighbors:
            raise Exception("This node is listed as a neighbor of itself")

        msg = frame.body
        return MessageTopologyReply(msg_id=self.next_msg_id, in_reply_to=msg.msg_id)

    def broadcast(self, frame) -> MessageBroadcastReply:
        msg = frame.body
        self.msg_log_broadcasts.add(msg.message)

        logger.debug(f"Forwarding Sync to neighbors: {self.neighbors}")
        message_broadcast = MessageBroadcastSync(msg_id=msg.msg_id, message=msg.message, informed=self.neighbors.union({self.node_id}))
        for neighbor in self.neighbors:
            logger.debug(f"Sending sync to {neighbor}")
            frame = Frame(src=self.node_id, dest=neighbor, body=message_broadcast)
            logger.trace(f"Sending {frame}")
            print(frame.json(), flush=True)

        return MessageBroadcastReply(msg_id=self.next_msg_id, in_reply_to=msg.msg_id)

    def broadcast_sync(self, frame):
        msg = frame.body
        self.msg_log_broadcasts.add(msg.message)

        needy_neighbors = self.neighbors.difference(msg.informed)
        logger.debug(f"Forwarding Sync to neighbors: {needy_neighbors}")
        message_broadcast = MessageBroadcastSync(msg_id=msg.msg_id, message=msg.message, informed=msg.informed.union(needy_neighbors))
        for neighbor in needy_neighbors:
            logger.debug(f"Sending sync to {neighbor}")
            frame = Frame(src=self.node_id, dest=neighbor, body=message_broadcast)
            logger.trace(f"Sending {frame}")
            print(frame.json(), flush=True)

    def read(self, frame) -> MessageReadReply:
        return MessageReadReply(msg_id=self.next_msg_id, in_reply_to=frame.body.msg_id, messages=sorted(self.msg_log_broadcasts))

    ####

    def __enter__(self, *_, **__):
        if self.node_id is not None:
            logger.info(f"Restarting node {self.node_id}")
            logger.debug(
                f"Node {self.node_id} has {len(self.msg_log_sent)} previous messages."
            )
        else:
            logger.info("Started uninitalized node")
        return self

    def __exit__(self, *_, **__):
        if self.node_id is not None:
            logger.info(f"Shutting down node {self.node_id}")
        else:
            logger.info(f"Shutting down uninitialized node")


####


def main():
    with Operation() as operation:
        for line in sys.stdin:
            try:
                reply = operation(line)
            except UninitializedNodeError:
                raise
            except NoReply:
                logger.debug("Not sending a response")
            else:
                print(reply.json(), flush=True)


if __name__ == "__main__":
    main()
